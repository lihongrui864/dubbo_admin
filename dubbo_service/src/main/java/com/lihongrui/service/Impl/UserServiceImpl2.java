package com.lihongrui.service.Impl;

import com.lihongrui.pojo.User;
import com.lihongrui.service.UserService;
import org.apache.dubbo.config.annotation.Service;

/**
 * @author lihongrui
 * @create 2022-12-03 14:26
 */
//@Service //将改类的对象创建出来，放到Spring的IOC容器中 bean的定义
@Service(timeout = 3000,retries = 2,version = "v2.0") //将这个类提供的方法(服务) 对外发布，将访问的地址ip 端口，路径注册到注册中心中
public class UserServiceImpl2 implements UserService {
    @Override
    public String sayHello() {
        return "hello-dubbo!~";
    }

    @Override
    public User findUserById(int id) {
        System.out.println("v2.0");
        //查询User对象
        User user = new User(1,"张三","123");
        //数据库查询很慢，查询5秒
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return user;
    }
}
