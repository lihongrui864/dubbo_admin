package com.lihongrui.pojo;

import java.io.Serializable;

/**
 * @author lihongrui
 * @create 2022-12-03 20:41
 * 注意：
 *  将来所有的pojo类都要实现 Serializable
 */
public class User implements Serializable {
    private int id;
    private String username;
    private String pasword;

    public User() {
    }

    public User(int id, String username, String pasword) {
        this.id = id;
        this.username = username;
        this.pasword = pasword;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasword() {
        return pasword;
    }

    public void setPasword(String pasword) {
        this.pasword = pasword;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", pasword='" + pasword + '\'' +
                '}';
    }
}
