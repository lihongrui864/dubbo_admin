package com.lihongrui.controller;


import com.lihongrui.pojo.User;
import com.lihongrui.service.UserService;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lihongrui
 * @create 2022-12-03 15:13
 * 启动这个demo时，需要运行两个服务一个消费方dubbo_web 一个提供方，dubbo_service
 */
@RestController
@RequestMapping("/user")
public class UserController {

    //注入Service
    //@Autowired //本地注入

    /*
    1,从zookeeper注册中心获取userService的访问url
    2,进行远程调用RPC
    3,将结果封装为一个代理对象，给变量赋值
    * */
    //远程注入
    //随机的负载均衡策略
//    @Reference(loadbalance = "random",cluster = "failover")
    @Reference(version = "v1.0")
    private UserService userService;

    @RequestMapping("/sayHello")
    public String sayHello(){
        return userService.sayHello();
    }

    @RequestMapping("/aaa")
    public void aaa(){
        System.out.println("请求没问题！");
    }
    /*
    根据id查询用户信息
    * */
    int i=1;
    @RequestMapping("/find")
    public User find(){
        new Thread(new Runnable() {
            public void run() {
                while (true){
                    System.out.println(i++);
                    try {
                        Thread.sleep(1000);
                    }catch (Exception e){
                        e.printStackTrace();
                    }
                }

            }
        }).start();
        User userById = userService.findUserById(1);
        return userById;
    }
}
