package com.lihongrui.service;

import com.lihongrui.pojo.User;

/**
 * @author lihongrui
 * @create 2022-12-03 14:25
 */
public interface UserService {
    public String sayHello();

    /*
    查询用户
    * */
    public User findUserById(int id);
}
